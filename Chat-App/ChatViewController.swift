//
//  ChatViewController.swift
//  Chat-App
//
//  Created by Elisha Narida on 10/08/2017.
//  Copyright © 2017 Imergex. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController
import Firebase

class ChatViewController: JSQMessagesViewController {
    
    var messages: [JSQMessage] = [] // array for JSQMessage
    
    var databaseRef: DatabaseReference? //database reference on firebase
    
    var newMessageRefHandle: DatabaseHandle? // new messages data handler
    var updatedMessageRefHandle: DatabaseHandle? // update messages data handler
    lazy var messageRef: DatabaseReference = Database.database().reference().child("messages") // location for data
    
    //Bubbles for chat
    // lazy when you want to use your variable
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        observeMessages() //implementation to function observeMessages
        
        //hide accesory button
        self.inputToolbar.contentView.leftBarButtonItem = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        senderId = Auth.auth().currentUser?.uid
        navigationItem.hidesBackButton = true // hides back button
        
        // avatar = nil
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
    }
    
    // MARK: Firebase related methods
    
    func observeMessages() {
        let messageQuery = messageRef.queryLimited(toLast:25)
        
        // observe to child added method to add the data messages to DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! Dictionary<String, String>
            
            if let id = messageData["senderId"] as String!,
                let name = messageData["senderName"] as String!,
                let text = messageData["text"] as String!,
                    text.characters.count > 0 {
                        self.addMessage(withId: id, name: name, text: text)
                        self.finishReceivingMessage()
                
            } else {
                print("Error! Could not decode message data from firebase")
            }
        })
    }
    
    deinit { //clean up
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {

        let itemRef = messageRef.childByAutoId()
        let messageItem = [
            "senderId": senderId!,
            "senderName": senderDisplayName!,
            "text": text!,
            ] //creates array
        
        itemRef.setValue(messageItem) //post itemRef to firebase
        finishSendingMessage()
        JSQSystemSoundPlayer.jsq_playMessageSentSound() // plays sound
    }
}

extension ChatViewController {
    
    // MARK: Collection view data source (and related) methods
    
    func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            messages.append(message)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId { // bubble for the user
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView //buble for the other user
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white // user id
        } else {
            cell.textView?.textColor = UIColor.black // other user id
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 10
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString? { //config for senderDisplayName
        let message = messages[indexPath.item]
        switch message.senderId {
        case senderId:
            return nil
        default:
            guard let senderDisplayName = message.senderDisplayName else {
                assertionFailure()
                return nil
            }
            return NSAttributedString(string: senderDisplayName)
        }
    }
    
    func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
}
