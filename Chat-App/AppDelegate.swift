//
//  AppDelegate.swift
//  Chat-App
//
//  Created by Elisha Narida on 10/08/2017.
//  Copyright © 2017 Imergex. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        return true
    }
    
    override init() { //firebase configuration
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
    }
}

