//
//  ViewController.swift
//  Chat-App
//
//  Created by Elisha Narida on 10/08/2017.
//  Copyright © 2017 Imergex. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet var txt_name: UITextField! // textfield on the storyboard
    let segueIdentifier = "ChatRoom" // string for segueIdentifier
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func EnterChatroomTapped(_ sender: Any) {
        validateNetwork()
        
    }
    
    func validateTextField() {
        
        if (txt_name.text?.isEmpty)! {
            print("name is empty")
            
        }else {
            
           Auth.auth().signInAnonymously(completion: { (user, error) in //sign in to firebase anonymously
                print("signed in anonymously")
                self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)  //present next view controller
            })
        }
    }
    
    func validateNetwork() {
        
        if Connectivity.isConnectedToInternet() { //if internet connection
            validateTextField()
            
        }else{
            print("no internet connectivity")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){ //when you performSegue this func will run
        if segue.identifier == segueIdentifier {
            if let vc = segue.destination as? ChatViewController {
                    vc.senderDisplayName = txt_name.text
            }
        }
    }
}

class Connectivity { //Module on Alamofire
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}



